Element["" "SOT23_3" "U?" "unknown" 0 0 0 0 0 50 ""] (
 Pad[-1mm -0.95mm -1.4mm -0.95mm 0.6mm 20mil 0.7524mm "" "1" "square"]
 Pad[-1mm 0.95mm -1.4mm 0.95mm 0.6mm 20mil 0.7524mm "" "2" "square"]
 Pad[1mm 0mm 1.4mm 0mm 0.6mm 20mil 0.7524mm "" "3" "square"]
 ElementLine[-1mm -1.425mm -1mm 1.425mm 5mil]
 ElementLine[-1mm 1.425mm 1mm 1.425mm 5mil]
 ElementLine[1mm 1.425mm 1mm -1.425mm 5mil]
 ElementLine[1mm -1.425mm -1mm -1.425mm 5mil]
)
