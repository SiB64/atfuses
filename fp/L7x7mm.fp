
Element["" "" "L?" "" 113.0000mm 16.0000mm -0.6000mm -0.8000mm 0 100 ""]
(
	Pad[-2.4000mm -2.4000mm 2.4000mm -2.4000mm 2.6000mm 20.00mil 3.1080mm "1" "1" "square"]
	Pad[-2.4000mm 2.4000mm 2.4000mm 2.4000mm 2.6000mm 20.00mil 3.1080mm "2" "2" "square"]
	ElementLine [-3.5000mm -3.5000mm 3.5000mm -3.5000mm 0.1500mm]
	ElementLine [3.5000mm -3.5000mm 3.5000mm 3.5000mm 0.1500mm]
	ElementLine [3.5000mm 3.5000mm -3.5000mm 3.5000mm 0.1500mm]
	ElementLine [-3.5000mm 3.5000mm -3.5000mm -3.5000mm 0.1500mm]
	ElementArc [0.0000 0.0000 2.5000mm 2.5000mm 180 90 0.1500mm]
	ElementArc [-0.0000mm -0.0000mm 2.5000mm 2.5000mm 90 90 0.1500mm]
	ElementArc [-0.0000mm 0.0000mm 2.5000mm 2.5000mm 0 90 0.1500mm]
	ElementArc [0.0000mm 0.0000mm 2.5000mm 2.5000mm 270 90 0.1500mm]

	)
