Element["" "LCC_1_4" "U?" "unknown" 0 0 0 0 0 50 ""] (
 Pad[-1.35mm -2.05mm -1.35mm -1.65mm 1.5mm 20mil 1.6524mm "" "1" "square"]
 Pad[-1.35mm 1.65mm -1.35mm 2.05mm 1.5mm 20mil 1.6524mm "" "2" "square"]
 Pad[1.35mm 1.65mm 1.35mm 2.05mm 1.5mm 20mil 1.6524mm "" "3" "square"]
 Pad[1.35mm -2.05mm 1.35mm -1.65mm 1.5mm 20mil 1.6524mm "" "4" "square"]
 ElementLine[-1.6mm -2.5mm -1.6mm 2.5mm 5mil]
 ElementLine[-1.6mm 2.5mm 1.6mm 2.5mm 5mil]
 ElementLine[1.6mm 2.5mm 1.6mm -2.5mm 5mil]
 ElementLine[1.6mm -2.5mm -1.6mm -2.5mm 5mil]
 ElementArc[-1.28mm -2.18mm -0.16mm -0.16mm 0 360 5mil]
)
