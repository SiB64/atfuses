Element["" "SC70_6" "U?" "unknown" 0 0 0 0 0 50 ""] (
 Pad[-0.6mm -0.65mm -0.999999mm -0.65mm 0.3mm 20mil 0.4524mm "" "1" "square"]
 Pad[-0.6mm 0mm -0.999999mm 0mm 0.3mm 20mil 0.4524mm "" "2" "square"]
 Pad[-0.6mm 0.65mm -0.999999mm 0.65mm 0.3mm 20mil 0.4524mm "" "3" "square"]
 Pad[0.6mm 0.65mm 0.999999mm 0.65mm 0.3mm 20mil 0.4524mm "" "4" "square"]
 Pad[0.6mm 0mm 0.999999mm 0mm 0.3mm 20mil 0.4524mm "" "5" "square"]
 Pad[0.6mm -0.65mm 0.999999mm -0.65mm 0.3mm 20mil 0.4524mm "" "6" "square"]
 ElementLine[-0.6mm -0.975mm -0.6mm 0.975mm 5mil]
 ElementLine[-0.6mm 0.975mm 0.6mm 0.975mm 5mil]
 ElementLine[0.6mm 0.975mm 0.6mm -0.975mm 5mil]
 ElementLine[0.6mm -0.975mm -0.6mm -0.975mm 5mil]
)
