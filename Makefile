#! /usr/bin/make -f

# the output file is large, symlink to some big space

OUT=./out

GNETLIST=lepton-netlist

default: $(OUT)/atfuses.dat

%.ckt: %_spice.sch
	$(GNETLIST) -g spice-sdb $< -o $@

$(OUT)/%.dat: %.ckt
	ngspice < $< | ./ngspice2gpt.awk > $@

