#! /usr/bin/gawk -f

/^[0-9]/ && I
/^Index/{
    I=1
    printf "%s", $1
    for (i=2; i<=NF; i++) {
	if (substr($i,1,2)=="v(") printf " %s", substr($i,3,length($i)-3)
	else printf " %s", $i
    }
    printf "\n"
}
