#! /usr/bin/python3

from fp import *

make(SOIC, (16,))
make(HEADER, (2,6,))
make(SOT, (3,5))
make(SC70, (6,))
part(SUBD, n=9, sex="F", female=True, backset=8.1*mm)
part(SOD, partname="SOD123")
part(SOD, partname="SOD523", polar=2)
part(SOD, partname="C0603")
part(SOD, partname="C1206")
part(SOD, partname="P1206")
